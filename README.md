Update:

Erkentnis dank https://codeberg.org/Joachim/youtubedlTippHilfe/issues/1 :

Wenn ich eine Liste von URLs hab, kann ich die youtube-dl mit -a direkt mitgeben. 
Fertig. 


Damit ist der Zweck dieses “Projekts” jetzt ‘nur’ noch, dass ich den Umgang mit Codeberg, git und python üben kann 

----

Idee:

-> Verschiedene Videos lokal in einem Verzeichnis haben. (z.B. Folgend von einer #KiKa-Serie)

Ansatz:

1. Eine Liste mit den URLs haben (urls.txt)

2. Daraus eine Liste (befehle.txt) mit den #youtubeDl-Befehlen erstellen
-> letzendlich einfach in jede Zeile:

* Vorne dran: youtube-dl
* hinten dran: -f [gewünschtes Format, z.B. MP4-512 oder 18]

3. chmod +x und ausführen. 


---

Auch wenn's nicht nötig ist(siehe oben), der skizzierte Plan ist grob umgesetzt mit url2youtubedl.py. 
Verwendung:

1. Vorraussetzung: urls.txt (muss so heißen!) liegt im gleichen Verzeichnis
2. python url2youtubedl.py > befehle.txt
3. chmod + x befehle.txt
4. ./befehle.txt

